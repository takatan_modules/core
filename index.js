const Takatan = require('./lib/factory')
require('./lib/core/abstract/factory.module')
const Core = require('./lib/core/core')
const Kefir = require('kefir')

module.exports = {
  Takatan,
  Log: Takatan.log,
  Kefir
}
