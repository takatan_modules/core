# @takatanjs/core

Core module for takatanjs system

Under development

## Intro

TakatanJS is nodejs CIMS (Content/Interfaces Management System). 
Homepage: https://js.takatan.tk - it's not so documented as I'd wished to, I'm sorry, but you can get some information there

This is TakatanJS 3.0 modules, redesigned to fix main TakatanJS 2.X problems - microservices support and others.

## Core

Core modules provides core layer modules and system functionality:

 - classes factory for multi-inheritance and mixes functionality
 - logging
 - modules management
 - core-workers rpc network with local/personal/broadcasting notifications/streams functionality for microservices design

## What will be added

Core modules are mostly defined, but defently would get some updates before "1.0"

 - updated modules management 

## Examples

There are some examples at `/examples/` folder:

### single_core

Example of starting simplest core, ready to listen for workers. Run `npm i` and `node server.js`

Project doesn't includes `takatanjs.json` file, so system do installation on 1st run.

### rpc_core and rpc_workers

Examples of core and worker communication. Contains "installed takatanjs" (with `takatanjs.json` file) with network configuration.

### user_module_core

Example of using additional private module with "project business logic". This is copy of `rpc_core` example, but logic moved to private module
