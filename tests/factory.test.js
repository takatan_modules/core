const { Takatan, Log, Kefir } = require('../index')

function testHasClass(name) {
  test('Factory should have '+name,()=>{
    const Cls = Takatan.class(name)
    expect(Cls).toBeDefined()
    let tp = typeof Cls
    expect(tp).toBe('function')
    expect(Cls.name).toBe(name)
  })
}

testHasClass('AbstractCoreObject')
testHasClass('AbstractFactory')
testHasClass('AbstractCoreModule')
testHasClass('AbstractFactoryCoreModule')
testHasClass('Core')

test('Factory class previously non registered',()=>{
  class Xyz {}
  const Cls = Takatan.class(Xyz)
  expect(Cls).toBe(Xyz)
})

test('Mix with object',()=>{
  Takatan.mix({
    mixWithObject() { return 10 }
  })
  const ACO = Takatan.extends('AbstractCoreObject')
  let aco = new ACO()
  expect(typeof aco.mixWithObject).toBe('function')
  expect(aco.mixWithObject()).toBe(10)
})

test('Mix for multiple classes',()=>{
  Takatan.mix(['AbstractCoreObject','AbstractFactory'],{
    mixToMultiple() { return 11 }
  })
  const ACO = Takatan.extends('AbstractCoreObject')
  let aco = new ACO()
  expect(typeof aco.mixToMultiple).toBe('function')
  expect(aco.mixToMultiple()).toBe(11)

  const AF = Takatan.extends('AbstractCoreObject')
  let af = new AF()
  expect(typeof af.mixToMultiple).toBe('function')
  expect(af.mixToMultiple()).toBe(11)
})

test('Mix to default  with class with constructor mixin',()=>{
  class MixClass {
    constructor()
    {
      this._mixData = 12
    }
    getMixClassData() { return this._mixData }
  }
  Takatan.mix(MixClass)

  const ACO = Takatan.extends('AbstractCoreObject')
  expect(ACO).toBeDefined()
  let aco = new ACO()
  expect(typeof aco.getMixClassData).toBe('function')
  expect(aco.getMixClassData()).toBe(12)
})

test('ReMix to some class with class with constructor mixin',()=>{
  class ReMixClass {
    constructor()
    {
      this._mixData = 15
    }
    reMixToFactory() { return 14 }
  }
  Takatan.mix('AbstractFactoryCoreModule',ReMixClass)

  const ACFM = Takatan.extends('AbstractFactoryCoreModule')
  expect(ACFM).toBeDefined()
  let acfm = new ACFM()
  expect(typeof acfm.getMixClassData).toBe('function')
  expect(typeof acfm.reMixToFactory).toBe('function')
  expect(acfm.getMixClassData()).toBe(15)
  expect(acfm.reMixToFactory()).toBe(14)
})

test('Previously runtime applyed mixes should exists',()=>{
  const AFCM = Takatan.extends('AbstractFactoryCoreModule')
  expect(AFCM).toBeDefined()
  let acfm = new AFCM()
  expect(typeof acfm.mixWithObject).toBe('function')
  expect(typeof acfm.mixToMultiple).toBe('function')
  expect(typeof acfm.getMixClassData).toBe('function')
  expect(typeof acfm.getMixClassData).toBe('function')
  expect(typeof acfm.reMixToFactory).toBe('function')
  expect(acfm.mixWithObject()).toBe(10)
  expect(acfm.mixToMultiple()).toBe(11)
  expect(acfm.getMixClassData()).toBe(15)
  expect(acfm.reMixToFactory()).toBe(14)
})

test('Core failing on no registered class',()=>{
  let core = Takatan('XYZ')
  expect(core).toBeNull()
})

test('Core singleton creating',()=>{
  let core = Takatan('Core')
  expect(typeof core).toBe('object')
  expect(typeof core.go).toBe('function')
  let core2 = Takatan('Core')
  expect(core2).toBe(core)
})
