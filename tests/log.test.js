const { Takatan, Log, Kefir } = require('../index')

test('Log object should have m method',()=>{
  expect(Log).toBeDefined()
  let tp = typeof Log.m
  expect(tp).toBe('function')
})
