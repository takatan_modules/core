const fs = require('fs')
const DT = require('directory-tree')
const DG = require('dependency-graph')
const Kefir = require('kefir')
const sjs = require('shelljs')
const path = require('path')

const F = require('../factory')
const log = F.log.m('ModuleManager')

/**
  Module Manager mix for fast access to any module
*/
F.mix({
  M(code) {
    if (this._core&&typeof this._core.module === 'function')
      return this._core.module(code)
    return null
  }
})

const AbstractCoreModule = require('../core/abstract/module')

/**
Core Module Manager class - provides modules loading, installing and so on
@extends module:core/core.AbstractCoreModule
*/
class ModuleManager extends F.extends(AbstractCoreModule)
{
  constructor(core)
  {
    super(core)

    this._useRequired = []

    this._modules = {
      module_manager: {
        info: null,
        module: this,
        installed: true,
        loaded: true,
        enabled: true
      }
    }
  }

  useModules(modules,configs=null)
  {
    let uses = modules
    if (!Array.isArray(modules))
      uses = [modules]
    this._useRequired = [...this._useRequired,...uses.map(m=>{
      let ret = {
        module: '',
        scope: '',
        origin: '',
        configs: configs
      }
      if (typeof m === 'object')
      {
        if (m.module)
          ret.module = m.module
        if (m.origin)
          ret.origin = m.origin
        if (m.scope)
          ret.scope = m.scope
      }
      if (typeof m === 'string')
      {
        let paths = m.split('/')
        if (paths.length===1)
        {
          //like use('some_user_module')
          ret.origin = 'user'
          ret.scope = 'user'
          ret.module = paths[0]
        }
        if (paths.length>=2)
        {
          //can be @origin/@scope[/module[/submodule]]
          //@origin === @takatanjs = @takatanjs module
          //@origin === @user = user ./modules module
          //@origin === @node = 3rd party module installed with npm to node_modules
          //
          // if origin not starts with @ = [@user/]@scope[/module[/submodule]]
          if (paths[0][0]==='@')
          {
            ret.origin = paths.shift().slice(1)
            ret.scope = paths.shift()
            ret.module = paths.join('/')
          } else {
            ret.origin = 'user'
            ret.scope = paths.shift()
            ret.module = paths.join('/')
          }
        }
      }
      if (!ret.origin.length||!ret.scope.length)
      {
        log.error('use failed!',m)
        return null
      }
      if (!['takatanjs','user','node'].includes(ret.origin))
      {
        log.error('use failed! origin unknown!',m)
        log.info('known origins:',['@takatanjs','@user','@node'])
        return null
      }
      return ret
    }).filter(x=>!!x)]
    if (this._useRequired.length)
      log.info('use required',this._useRequired)
  }

  isInstalled()
  {
    let f = process.cwd()+'/takatanjs.json'
    if (!fs.existsSync(f))
      return false
    let data = null
    try {
      data = JSON.parse(fs.readFileSync(f).toString())
    } catch (e) {
      return false
    }
    if (data&&data.core)
      return true
    return false
  }

  _clddir2tree(el,scope,origin,prnt='')
  {
    let jsonPath = el.path+'/'+el.name+'.json'
    let info = null
    try {
      info = JSON.parse(fs.readFileSync(jsonPath).toString())
    } catch(e) {
      info = null
    }
    if (!info)
      return null
    if (!fs.existsSync(el.path+'/module.js'))
      return null
    if (!Array.isArray(info.deps))
      info.deps = []
    if (!Array.isArray(info.npm))
      info.npm = []
    if (typeof info.autoload === 'undefined')
      info.autoload = true
    let ret = {
      code: el.name,
      fullCode: el.name,
      path: jsonPath,
      dir: el.path,
      info: info,
      scope: scope,
      origin: origin,
      parent: prnt,
      childs: [],
      deps: info.deps,
      npm: info.npm
    }
    if (prnt&&prnt.length)
    {
      ret.fullCode = prnt+'/'+ret.code
      if (!ret.deps.includes(prnt))
        ret.deps = [prnt,...info.deps]
    }
    let retlist = []
    if (Array.isArray(info.childs)&&info.childs.length)
    {
      let retChilds = []
      info.childs.map(chld=>{
        let cldEl = el.children.reduce((acc,x)=>{
          if (acc) return acc
          if (x.type==='directory'&&x.path===ret.dir+'/'+chld)
            return x
          return acc
        },null)
        if (!cldEl)
        {
          log.warn('child defined in module but implementation not found',{code:el.name,child:chld})
          return null
        }
        let childslist = this._clddir2tree(cldEl,scope,origin,ret.fullCode)
        if (Array.isArray(childslist))
          childslist.forEach(x=>{
            retlist.push(x)
            retChilds.push(x)
          })
      })
      ret.childs = retChilds
    }
    return [ret,...retlist]
  }

  _scanModulesDir(path,scope,origin)
  {
    let dir = DT(path,{extensions:/\.json/})
    let ret = []
    if (dir&&Array.isArray(dir.children))
      dir.children.forEach(cld=>{
        let list = this._clddir2tree(cld,scope,origin)
        if (!Array.isArray(list))
        {
          log.warn('not a module!',cld.path)
          return
        }
        ret = [...ret,...list]
      })
    return ret
  }

  _dirGraph(path,scope,origin)
  {
    log.log('building modules graph',path)
    let list = this._scanModulesDir(path,scope,origin)
    let graph = new DG.DepGraph()
    //loading graph nodes
    list.forEach(m=>{
      graph.addNode(m.fullCode)
      //if it's not core scope
      //we should load known modules to graph
      //to satisfy dependences on core or other existing modules
      if (scope!=='core')
      {
        //we just push already existing modules
        //without care of there dependencies
        //because they are already loaded,
        //so theirs dependencies are satisfied, for sure
        Object.keys(this._modules).forEach(mc=>{
          graph.addNode(m.fullCode)
        })
      }
    })
    //loading dependencies
    list.forEach(m=>{
      m.deps.forEach(d=>{
        graph.addDependency(m.fullCode,d)
      })
    })
    graph._list = list
    return graph
  }

  _installGraph(scope,graph)
  {
    let $load = []
    let $install = []
    let $defaults = []
    log.log('install graph order',graph.overallOrder())
    graph.overallOrder().forEach(mcode=>{
      let mnode = graph._list.reduce((acc,x)=>x.fullCode===mcode?x:acc,null)
      if (!mnode)
      {
        log.error('install graph node not found in graph _list!',mcode)
        return
      }
      $load.push(this.loadModule(mnode,true))
      $install.push(this.installModule(mcode).flatMap(cfg=>this.configureModule(mcode,cfg)))
      $defaults.push(this.installModuleDefaults(mcode))
    })
    return Kefir.concat([...$load,...$install,...$defaults])
    .takeErrors(1)
    .last()
  }

  installCore()
  {
    //let's get one lvl up relative to our current path
    let graph = this._dirGraph(path.join(__dirname,'..'),'core','takatanjs')
    return this._installGraph('core',graph)
  }

  /**
    opts.enabled = true
    opts.noenv = false
    opts.start = true
  */
  installNewModule(code,config=null,opts=null)
  {
    return Kefir.constant(this._loadOrigins())
    .flatMap(graph=>{
      let minfo = graph._list.reduce((acc,x)=>x.fullCode===code?x:acc,null)
      if (!minfo)
        return Kefir.constantError({code:'module_not_found',module:code})

      if (!opts) opts = {
        enabled:true,
        start: true
      }
      let $t = [
        this.loadModule(minfo,true),
        this.registerModule(code),
        this.installModule(code,{
          enabled: opts.enabled,
          noenv: opts&&opts.noenv,
          config: config
        }).flatMap(cfg=>this.configureModule(code,cfg))
      ]
      if (opts.start)
      {
        $t.push(this.initModule(code))
        $t.push(this.startModule(code))
      }
      return Kefir.concat($t)
    })
    .last()
    .takeErrors(1)
    .map(_=>true)
  }

  _list2graph(list)
  {
    //console.log(['_list2graph',list])
    let graph = new DG.DepGraph()
    graph._satisfied = true
    graph._list = list
    //loading graph nodes
    list.forEach(m=>{
      graph.addNode(m.fullCode)
    })
    //loading dependencies
    list.forEach(m=>{
      m.deps.forEach(d=>{
        if (!graph.hasNode(m.fullCode)||!graph.hasNode(d))
        {
          log.warn('module dependencies unsatisfied!',[m.fullCode,d])
          graph._satisfied = false
          return
        }
        graph.addDependency(m.fullCode,d)
      })
    })
    //this._lastInfoGraph = graph
    return graph
  }

  _buildInfoGraph(path,scope,origin)
  {
    let list = this._scanModulesDir(path,scope,origin)
    return this._list2graph(list)
  }

  _loadOrigins()
  {
    let corePath = path.join(__dirname,'..')
    let $t = [this._buildInfoGraph(corePath,'core','takatanjs')]
    
    let unfoundUse = []

    let tDT = DT(path.join(process.cwd(),'/node_modules/@takatanjs'))
    tDT.children.forEach(bundleDir=>{
      if (bundleDir.name==='core') return //core already done
      let libPath = path.join(bundleDir.path,'/lib')
      if (fs.existsSync(libPath)&&fs.lstatSync(libPath).isDirectory())
      {
        log.info('found @takatanjs/ bundle',bundleDir.name)
        $t.push(this._buildInfoGraph(libPath,bundleDir.name,'takatanjs'))
      } else {
        log.warn('@takatanjs/???? has no lib',bundleDir.name)
      }
    })
    //right now it's only user modules
    $t.push(this._buildInfoGraph(path.join(process.cwd(),'/modules'),'user','user'))
    let originsList = $t.reduce((acc,x)=>{
      return [...acc,...x._list]
    },[])
    let ret = this._list2graph(originsList)
    return ret
  }

  _loadAndStartMDB(infoGraph)
  {
    let mdb = this._readMDB()
    let $load = []
    let $install = []
    let $configure = []
    let $register = []
    let $init = []
    let $start = []
    let $updates = []

    let toLoad = 0
    let toInstall = 0

    /**/
    infoGraph.overallOrder().forEach(mcode=>{
      let mnode = infoGraph._list.reduce((acc,x)=>x.fullCode===mcode?x:acc,null)
      if (!mnode)
      {
        log.error('load graph node not found in graph _list!',mcode)
        return
      }
      //skip installed and not enabled module
      if (mdb&&mdb[mnode.scope]&&mdb[mnode.scope][mcode]&&mdb[mnode.scope][mcode].installed&&!mdb[mnode.scope][mcode].enabled)
        return
      if (!mdb||!mdb[mnode.scope]||!mdb[mnode.scope][mcode]||!mdb[mnode.scope][mcode].installed)
      {
        log.debug('module found but not installed',{scope:mnode.scope,module:mcode,origin:mnode.origin})
        //check if there are required but not installed modules
        this._useRequired.forEach(ur=>{
          //console.log('use required',ur,mnode)
          if (ur.scope===mnode.scope&&ur.origin===mnode.origin)
          {
            if (((ur.module===''||ur.module==='*')&&mnode.info.autoload)||ur.module===mcode)
            {
              let cfg = ur.configs && ur.configs[mcode]? ur.configs[mcode] : null
              log.info('module is required but not installed. installing',mcode)
              //console.log(['use require cfg',ur.configs[mcode],mcode])
              toLoad++
              toInstall++
              $load.push(this.loadModule(mnode,true))
              $register.push(this.registerModule(mcode))
              $install.push(this.installModule(mcode,{config:cfg})
                .flatMap(cfg=>this.configureModule(mcode,cfg))
                .flatMap(_=>this.installModuleDefaults(mcode))
                .flatMap(_=>this.initModule(mcode))
              )
              $start.push(this.startModule(mcode))
            }
          }
        })
        return
      }
      toLoad++
      let cfg = null
      if (mdb&&mdb[mnode.scope]&&mdb[mnode.scope][mcode]&&mdb[mnode.scope][mcode].config)
        cfg = mdb[mnode.scope][mcode].config
      //console.log('load and start mdb... module cfg',mcode,cfg)
      if (mnode.scope==='core')
      {
        //core modules register and configured on load
        $load.push(this.loadModule(mnode)
          .flatMap(_=>this.configureModule(mcode,cfg))
        )
      } else {
        $load.push(this.loadModule(mnode))
        $configure.push(this.configureModule(mcode,cfg))
      }
      $register.push(this.registerModule(mcode))
      $init.push(this.initModule(mcode))
      $start.push(this.startModule(mcode))
      $updates.push(this.updateModule(mcode,mnode))
    })
    log.info('starting with modules',{load:toLoad,total:infoGraph.overallOrder().length,install:toInstall})

    return Kefir.concat([
      ...$load,
      ...$register,
      ...$configure,
      ...$updates,
      ...$init,
      ...$install,
      ...$start
    ]).takeErrors(1).last()
  }

  startNormally()
  {
    return this._loadAndStartMDB(this._loadOrigins())
  }

  _getUpdateVersion(ver)
  {
    if (ver.split('.').length===3)
      return parseInt(ver.split('.').pop())
    return 0
  }


  moduleExists(code) { return typeof this._modules[code] != 'undefined' }
  moduleLoaded(code) {
    return this.moduleExists(code)&&this._modules[code].loaded
  }
  moduleLoadedStream(code,errorCode) {
    return Kefir.stream(obs=>{
      if (!this.moduleLoaded(code))
      {
        log.error('module not loaded!',code)
        return this.obsErr(obs,{code:errorCode,module:code})
      }
      this.obsEnd(obs,this.moduleObject(code))
    })
  }
  moduleInstalled(code) {
    return this.moduleExists(code)&&this._modules[code].installed
  }
  moduleEnabled(code) {
    return this.moduleExists(code)&&this._modules[code].enabled
  }
  moduleObject(code) {
    if (!this.moduleExists(code)) return null
    return this._modules[code].module
  }

  loadModule(info,installNpm=false)
  {
    return Kefir.stream(obs=>{
      if (info.fullCode==='module_manager'&&info.scope==='core')
      {
        this._modules.module_manager.info = info
        this.moduleLoad().observe(_=>{
          if (typeof this.$notify === 'function'&&this.M('events'))
            this.$notify('@module_manager.module_loaded','module_manager')
          this.obsEnd(obs,info)
        },err=>{
          console.log(err)
          this.obsErr(obs,err)
        })
        return
      }
      log.log('loading module',info.fullCode)
      //check module npm
      let nx = Kefir.constant(true)
      if (installNpm&&Array.isArray(info.info.npm)&&info.info.npm.length)
      {
        log.test('installing module npm before load',info.fullCode)
        nx = this._installModuleNpm(info.info.npm)
      }
      nx.observe(()=>{
        //console.log('check npm before load',info)
        /**/
        let MCls = null
        try {
          MCls = require(path.join(info.dir,'/module'))
        } catch(e) {
          log.error('requiring module class failed')
          console.log(e)
          return this.obsErr(obs,{code:'MODULE_MANAGER.MODULE_CLASS_REQUIRE_ERROR',module:info.fullCode})
        }
        if (!F.class('AbstractCoreModule').isPrototypeOf(MCls))
        {
          log.error('module class is not inherited from AbstractCoreModule')
          return this.obsErr(obs,{code:'MODULE_MANAGER.MODULE_CLASS_PROTOTYPE_ERROR',module:info.fullCode})
        }
        log.log('creating module object',info.fullCode)
        let md = new MCls()
        md.coreObjectSetCore(this._core)
        md.moduleReInfo({
          ...info.info,
          fullCode: info.fullCore,
          scope: info.scope
        })
        md.moduleLoad().observe(_=>{
          log.info('module loaded',[info.fullCode,info.info.version])
          let mdb = this._readMDB()
          let inf = mdb[info.scope] && mdb[info.scope][info.fullCode] ? mdb[info.scope][info.fullCode] : null
          //console.log('module load',info,mdb[info.scope][info.fullCode])
          this._modules[info.fullCode] = {
            info: info,
            module: md,
            loaded: true,
            installed: inf && inf.installed ? inf.installed : false,
            enabled: inf && inf.enabled ? inf.enabled : false
          }
          if (typeof this.$notify === 'function'&&this.M('events'))
            this.$notify('@module_manager.module_loaded',info.fullCode)
          this.obsEnd(obs,info)
        },err=>{
          console.log(err)
          this.obsErr(obs,err)
        })
      },err=>{
        log.error('installing module required npm failed',err)
        this.obsErr(obs,{code:'module_manager.install_npm_error',error:err})
      })
      /**/
    })
  }

  _installModuleNpm(list)
  {
    return Kefir.stream(obs=>{
      log.test('installing required npm modules',list)
      sjs.exec('npm i -s '+list.join(' '),(code,out,err)=>{
        if (code===0&&(!err||!err.length))
        {
          log.success('npm modules installed!',list)
          return this.obsEnd(obs,true)
        }
        log.error('npm modules installation error',{code,err})
        //console.log(['sjs result',code,out,err])
        this.obsErr(obs,{code:'module_manager.npm_install_error',npm:list,stdErr:err,code:code})
      })
    })
  }

  /**
    opts.config = module config
    opts.enabled = true/[false] - module state
    opts.onlyMDB = true/[false] - don't call module routines, only make changes at takatan.json
    opts.noenv = true/[false] - don't use env variables for module config
  */
  installModule(mcode,opts={})
  {
    return this.moduleLoadedStream(mcode,'MODULE_MANAGER.INSTALL_MODULE_NOT_LOADED')
      .flatMap(mobj=>{
        log.warn('installing module',mcode)

        let mdb = this._readMDB()
        let minfo = this._modules[mcode].info
        if (typeof mdb[minfo.scope] === 'undefined')
          mdb[minfo.scope] = {}
        mdb[minfo.scope][mcode] = {
          version: minfo.info.version,
          installed: false,
          enabled: false,
          config: null
        }
        if (opts&&opts.enabled)
          mdb[minfo.scope][mcode].enabled = true

        let inCfg = opts&&opts.config?opts.config:null
        if (opts&&!opts.noenv)
          inCfg = this._mapEnvConfig(mcode,inCfg)

        if (opts&&opts.onlyMDB)
        {
          mdb[minfo.scope][mcode].installed = true
          mdb[minfo.scope][mcode].config = inCfg
          this._writeMDB(mdb)
          if (typeof this.$notify === 'function'&&this.M('events'))
            this.$notify('@module_manager.module_installed',code)
          return Kefir.constant(mdb[minfo.scope][mcode].config)
          //return Rx.of(mdb[minfo.scope][mcode].config)
        }
        this._writeMDB(mdb)

        return mobj.moduleInstall(inCfg)
          .map(modCfg=>{
            log.log('module install routine complete!',mcode)
            let mdb2 = this._readMDB()
            mdb2[minfo.scope][mcode].installed = true
            mdb2[minfo.scope][mcode].enabled = true
            mdb2[minfo.scope][mcode].config = modCfg
            this._writeMDB(mdb2)
            this._modules[mcode].installed = true
            if (typeof this.$notify === 'function'&&this.M('events'))
              this.$notify('@module_manager.module_installed',mcode)
            return mdb2[minfo.scope][mcode].config
          })

      })
  }

  installModuleDefaults(mcode)
  {
    return Kefir.stream(obs=>{
      if (!this.moduleInstalled(mcode))
      {
        log.error('module not installed!',mcode)
        return this.obsErr(obs,{code:'MODULE_MANAGER.INSTALL_DEFAULTS_MODULE_NOT_INSTALLED',module:mcode})
      }
      log.info('install module defaults',mcode)
      this.obsEnd(obs,this.moduleObject(mcode))
    })
      .flatMap(m=>m.moduleDefaults())
      .map(_=>{
        if (typeof this.$notify === 'function'&&this.M('events'))
          this.$notify('@module_manager.module_defaults_installed',mcode)
        return _
      })
  }

  _mapEnvConfig(code,conf)
  {
    let inf = this._modules[code].info
    if (!Array.isArray(inf.info.confs)||!inf.info.confs.length)
      return conf
    inf.info.confs.forEach(cfg=>{
      let ecode = inf.origin+'_'+inf.scope+'_'+code.split('/').join('_')+'_'+cfg.code
      ecode = ecode.toUpperCase()
      if (typeof process.env[ecode] !== 'undefined')
      {
        if (!conf) conf = {}
        conf[cfg.code] = process.env[ecode]
        if (cfg.type==='number')
          conf[cfg.code] = parseInt(conf[cfg.code])
        if (cfg.type==='float')
          conf[cfg.code] = parseFloat(conf[cfg.code])
      }
    })
    return conf
  }

  /**
    opts.noenv = true - don't use env
    opts.saveMDB = true - update takatan.json
  */
  configureModule(code,conf=null,opts=null)
  {
    return Kefir.stream(obs=>{
      if (!this.moduleInstalled(code))
      {
        log.error('configure, module not installed!',code)
        return this.obsErr(obs,{code:'MODULE_MANAGER.CONFIGURE_MODULE_NOT_INSTALLED',module:code})
      }
      log.log('configuring module',{code,conf})
      this.obsEnd(obs,this.moduleObject(code))
    })
    .flatMap(m=>{
      if (opts&&!opts.noenv)
        conf = this._mapEnvConfig(code,conf)
      if (opts&&opts.saveMDB)
      {
        let minfo = this._modules[code].info
        let mdb2 = this._readMDB()
        mdb2[minfo.scope][code].config = conf
        this._writeMDB(mdb2)
      }
      return m.moduleConfigure(conf)
    })
    .map(_=>{
      log.log('module configured',code)
      if (typeof this.$notify === 'function'&&this.M('events'))
        this.$notify('@module_manager.module_configured',code)
      return _
    })
  }

  updateModule(mcode,mnode)
  {
    return this.moduleLoadedStream(mcode,'MODULE_MANAGER.MODULE_REGISTER_MODULE_NOT_LOADED')
      .flatMap(mod=>{
        let mdb = this._readMDB()
        let curVersion = mdb[mnode.scope][mcode].version
        let reqVersion = mnode.info.version
        let curUpdate = this._getUpdateVersion(curVersion)
        let reqUpdate = this._getUpdateVersion(reqVersion)

        //no update required
        if (curVersion===reqVersion)
          return Kefir.constant(true)

        log.info('module update required',{module:mcode,from:curVersion,to:reqVersion,updates:reqUpdate-curUpdate})

        let $updates = []
        for (let i = curUpdate + 1; i<=reqUpdate; i++)
        {
          if (typeof mod[`moduleUpdateTo${i}`] === 'function')
          {
            $updates.push(mod[`moduleUpdateTo${i}`]()
              .mapErrors(err=>{
                log.error('module update failed',{mcode,curVersion,reqVersion,i})
                return {code:'module_manager.module_update_failed',mcode,updateTo:i}
              })
              .flatMap(updRes=>{
                if (updRes) {
                  log.success('module updated',{module:mcode,updatedTo:i})
                  if (typeof this.$notify === 'function'&&this.M('events'))
                    this.$notify('@module_manager.module_updated',{module:mcode,updatedTo:i})
                  let nextVer = curVersion.split('.')
                  nextVer[2] = `${i}`
                  mdb[mnode.scope][mcode].version = nextVer.join('.')
                  if (i===reqUpdate)
                    mdb[mnode.scope][mcode].version = reqVersion
                  this._writeMDB(mdb)
                  return Kefir.constant(true)
                }
                log.error('module update failed! runing system may be unpredictable! it\'s STRONG RECOMMENDED to stop system, and manualy fix update problem before start',{module:mcode,moduleScope:mnode.scope,currentVersion:curVersion,requiredVersion:reqVersion,updatesRequired:$updates.length,failedOn:i})
                return Kefir.constant(false)

              })
            )
          }
        }
        if (!$updates.length)
        {
          mdb[mnode.scope][mcode].version = reqVersion
          this._writeMDB(mdb)
          log.success('module updated without special update routines',{module:mcode,from:curVersion,to:reqVersion})
          this.$notify('@module_manager.module_updated',{module:mcode,updatedTo:reqUpdate})
          return Kefir.constant(true)
        }
        return Kefir.concat($updates).takeWhile(x=>!!x)
      })
    /**/
  }

  registerModule(code)
  {
    return this.moduleLoadedStream(code,'MODULE_MANAGER.MODULE_REGISTER_MODULE_NOT_LOADED')
      .flatMap(m=>m.moduleRegister())
      .map(_=>{
        log.log('module register routine complete',code)
        if (typeof this.$notify === 'function'&&this.M('events'))
          this.$notify('@module_manager.module_registered',code)
        return _
      })
  }


  initModule(code)
  {
    return this.moduleLoadedStream(code,'MODULE_MANAGER.MODULE_INIT_MODULE_NOT_LOADED')
      .flatMap(m=>m.moduleInit())
      .map(_=>{
        log.log('module initialized',code)
        if (typeof this.$notify === 'function'&&this.M('events'))
          this.$notify('@module_manager.module_initialized',code)
        return _
      })
    /**/
  }

  startModule(code)
  {
    return this.moduleLoadedStream(code,'MODULE_MANAGER.MODULE_START_MODULE_NOT_LOADED')
      .flatMap(m=>m.moduleStart())
      .map(_=>{
        log.log('module started',code)
        if (typeof this.$notify === 'function'&&this.M('events'))
          this.$notify('@module_manager.module_started',code)
        return _
      })
    /**/
  }

  _readMDB()
  {
    if (this._mdb)
      return this._mdb
    let db = null
    try {
      db = JSON.parse(fs.readFileSync(process.cwd()+'/takatanjs.json').toString())
    } catch(e) {
      db = null
    }
    if (!db)
      db = {
        core: {}
      }
    this._mdb = db
    return db
  }

  _writeMDB(db)
  {
    this._mdb = db
    fs.writeFileSync(process.cwd()+'/takatanjs.json',JSON.stringify(db,null,2))
  }

}

module.exports = F.register(ModuleManager)
