const F = require('../factory')
const log = F.log.m('AckErrorEvent')

const AckEvent = require('./event.ack')

class AckErrorEvent extends F.extends(AckEvent)
{

  static info()
  {
    return {
      code: 'ackerror',
      name: 'Ack error system event',
      description: 'Ack error system event object contains stream error'
    }
  }
  
}

module.exports = F.register(AckErrorEvent)
