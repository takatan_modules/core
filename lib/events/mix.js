const Kefir = require('kefir')

const F = require('../factory')
const log = F.log.m('EventsMix')

module.exports = {
  $notify(code,data)
  {
    let ev = this.M('events')._parseCode(code)
    if (this.M('events'))
      this.M('events').pushNotification(this,{...ev,data})
  },
  $eon(code)
  {
    return this.M('events').getNotificationPool(code)
  },
  $on(code)
  {
    return this.$eon(code)
      .map(e=>e?e.data():undefined)
  },
  $ack(code,data)
  {
    let ev = this.M('events')._parseCode(code)
    return this.M('events').pushAck(this,{...ev,data},true)
  },
  $eack(code,data)
  {
    let ev = this.M('events')._parseCode(code)
    return this.M('events').pushAck(this,{...ev,data})
  },
  $stream(code,fn)
  {
    this.M('events').setStream(this,code,fn,true)
  },
  $estream(code,fn)
  {
    this.M('events').setStream(this,code,fn)
  }
}
