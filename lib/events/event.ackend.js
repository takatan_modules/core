const F = require('../factory')
const log = F.log.m('AckEndEvent')

const AckEvent = require('./event.ack')

class AckEndEvent extends F.extends(AckEvent)
{

  static info()
  {
    return {
      code: 'ackend',
      name: 'Ack end system event',
      description: 'Ack end system event object contains stream end event'
    }
  }

}

module.exports = F.register(AckEndEvent)
