const F = require('../factory')
const log = F.log.m('NotificationEvent')

const AbstractEvent = require('./abstract')

class NotificationEvent extends F.extends(AbstractEvent)
{

  static info()
  {
    return {
      code: 'notification',
      name: 'Notification system event',
      description: 'Notification system event object doesn\' require receiver and reply'
    }
  }

}

module.exports = F.register(NotificationEvent)
