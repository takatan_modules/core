const Kefir = require('kefir')
const AbstractFactoryCoreModule = require('../core/abstract/factory.module')

const F = require('../factory')
const log = F.log.m('EventsModule')

const AbstractEvent = require('./abstract')
const NotificationEvent = require('./event.notification')
const AckEvent = require('./event.ack')
const AckValueEvent = require('./event.ackvalue')
const AckErrorEvent = require('./event.ackerror')
const AckEndEvent = require('./event.ackend')

const EventsMix = require('./mix')

F.mix(EventsMix)

class EventsModule extends F.extends(AbstractFactoryCoreModule)
{
  constructor(core)
  {
    super(core)

    this.factoryInit('eventTypes',AbstractEvent)
    this.factoryRegisterClass('eventTypes',NotificationEvent)
    this.factoryRegisterClass('eventTypes',AckEvent)
    this.factoryRegisterClass('eventTypes',AckValueEvent)
    this.factoryRegisterClass('eventTypes',AckErrorEvent)
    this.factoryRegisterClass('eventTypes',AckEndEvent)

    this._lastAck = 0
    this._acks = {}
    this._ackReplays = {}
    this._ackFns = {}

    this._events = {
      '#': {},
      '@': {},
      '@@': {},
      'core': {}
    }
  }

  _isCore()
  {
    return this.M('rpc')&&this.M('rpc').rpcCode()==='core'
  }

  _parseCode(code)
  {
    let scope = '#'
    let ecode = code
    //simple case
    if (code.startsWith('#'))
    {
      ecode = code.slice(1)
    }
    //ok, send somewhere?
    if (code.includes('@'))
    {
      //to core or broadcast
      if (code.startsWith('@'))
      {
        if (code.startsWith('@@'))
        {
          scope = '@@'
          ecode = code.slice(2)
        } else {
          scope = '@'
          ecode = code.slice(1)
        }
      } else {
        let splt = code.split('@')
        if (splt.length!==2)
        {
          log.error('event build error!',code)
          return
        }
        scope = splt[0]
        ecode = splt[1]
      }
    }
    return {
      scope: scope,
      code: ecode
    }
  }

  /* notifications */

  getNotificationPool(code)
  {
    let ev = this._parseCode(code)
    if (typeof this._events[ev.scope] === 'undefined')
      this._events[ev.scope] = {}
    if (typeof this._events[ev.scope][ev.code] === 'undefined')
      this._events[ev.scope][ev.code] = Kefir.pool()
    return this._events[ev.scope][ev.code]
  }

  /**
    ev = eventInfo = {
      type: 'eventType', //event type defines event Class from factory
      code: 'eventCode', //event code is main event identifier
      data: {} || undefined, //event data if exists
      scope: '#' || '@' || '@@' || '{core|worker}' //scope defines recievers of event
    }
  */
  pushNotification(sender,ev)
  {
    const EventClass = this.factoryClass('eventTypes','notification')
    let e = new EventClass(sender)

    //log.trace('pushEvent',{ev})

    e.fill({
      code: ev.code,
      data: ev.data
    },ev.scope)

    //on early initialization rpc module still not exists
    if (!this.M('rpc'))
      e.reScope('#')

    let to = e.scope()

    //to core, but we are core
    if ((to==='core'||to==='@')&&this._isCore())
      to = '@'

    //push localy only if we have subscribers
    if ((to==='#'||to==='@')&&(this._events[to] && this._events[to][e.code()]))
      this._events[to][e.code()].plug(Kefir.constant(e))

    // if scope is local or
    // to core required, but this is core process
    if (to === '#'||(to==='@'&&this._isCore()))
      return

    //so, sender is remote [also]
    this.M('rpc').sendNotificationEvent(e)
  }

  netNotification(e)
  {
    log.log('network event',e.toObject())
    //can't get local scope from net
    if (e.scope()==='#')
    {
      log.error('got # scope event from net!')
      return
    }
    //global scope events are pushed only at global
    if (e.scope()==='@@')
    {
      if (this._events[e.scope()][e.code()])
        this._events[e.scope()][e.code()].plug(Kefir.constant(e))
      return
    }
    //income event with scope @ can be only at core
    if (e.scope()==='@')
    {
      if (this.M('rpc').rpcCode()!=='core')
      {
        log.error('got @ scope event but not core!')
        return
      }
      //incoming events for core we manage at sender
      if (this._events[e.sender().rpc]&&this._events[e.sender().rpc][e.code()])
        this._events[e.sender().rpc][e.code()].plug(Kefir.constant(e))
      //and at @
      if (this._events['@'][e.code()])
        this._events['@'][e.code()].plug(Kefir.constant(e))
      return
    }
    //so this should be message to our process
    if (e.scope()!==this.M('rpc').rpcCode())
    {
      log.error('event from network, but no idea why here',[e.toObject(),this.M('rpc').rpcCode()])
      return
    }
    //log.trace('network event',[e.scope(),e.sender().rpc,e.code()])
    //incoming events for us we manage at sender
    if (this._events[e.sender().rpc]&&this._events[e.sender().rpc][e.code()])
      this._events[e.sender().rpc][e.code()].plug(Kefir.constant(e))
    //and at @ for core
    if (e.sender().rpc==='core'&&this._events['@'][e.code()])
      this._events['@'][e.code()].plug(Kefir.constant(e))
  }

  /* streams */

  pushAck(sender,ev,dataOnly=false)
  {
    return Kefir.stream(obs=>{
      if (ev.scope === '@@')
      {
        log.error('stream can not be broadcast',{ev,sender})
        return this.obsErr(obs,{code:'events.boradcast_ack_error'})
      }

      const EventClass = this.factoryClass('eventTypes','ack')
      let e = new EventClass(sender)

      e.fill({
        code: ev.code,
        data: ev.data
      },ev.scope)

      //local ack
      //rpc is not involved
      if (e.scope() === '#')
      {
        log.log('requesting ack locally',{code:e.code(),scope:e.scope()})
        let ret = this._ackFns['#'][e.code()]
        if (!ret)
          return this.obsErr(obs,{code:'events.ack_not_found'})
        log.info('local ack result stream')
        //console.log(ret)
        let arg = e
        if (dataOnly)
          arg = e.data()
        ret.fn(arg).observe(sVal=>{
          if (ret.dataOnly)
          {
            obs.emit(sVal)
            return
          }
          let sev = this.M('events').eventFromAckEvent(ret.sender,'value',e)
          sev.data(sVal)
          obs.emit(sev)
        },sErr=>{
          if (ret.dataOnly)
          {
            obs.error(sErr)
            return
          }
          let sev = this.M('events').eventFromAckEvent(ret.sender,'error',e)
          sev.data(sErr)
          obs.error(sev)
        },sEnd=>{
          obs.end()
        })
      } else {
        log.log('requesting ack on rpc',{code:e.code(),scope:e.scope()})
        //log.trace('requesting ack on rpc',{e.code(),e.scope()})
        this._lastAck++
        e.ackId(this._lastAck)
        this.M('rpc').sendStreamAckEvent(e).observe(res=>{
          this._ackReplays[e.ackId()] = {obs,dataOnly}
        },err=>{
          log.error('rpc ack error',err)
          obs.error(err)
          obs.end()
        })
      }
    })
  }

  netAckStream(e)
  {
    log.log('ackRequest',e.toObject())
    //check at sender scope
    //console.log([e.toObject(),this._ackFns])
    if (this._ackFns[e.sender().rpc]&&this._ackFns[e.sender().rpc][e.code()])
      return this._ackFns[e.sender().rpc][e.code()]
    //check at @ scope if event from core
    if (e.sender().rpc==='core'&&this._ackFns['@']&&this._ackFns['@'][e.code()])
      return this._ackFns['@'][e.code()]
    //check at @ scope if event to core
    if ((e.scope()==='core'||e.scope()==='@')&&this._isCore()&&this._ackFns['@']&&this._ackFns['@'][e.code()])
      return this._ackFns['@'][e.code()]
    //check at @@ (broadcast===from any) scope if still nothing found
    if (this._ackFns['@@']&&this._ackFns['@@'][e.code()])
      return this._ackFns['@@'][e.code()]
    return null
  }


  setStream(sender,code,fn,dataOnly=false)
  {
    let ev = this._parseCode(code)
    if (!this._ackFns[ev.scope])
      this._ackFns[ev.scope] = {}
    if (!this._ackFns[ev.scope][ev.code])
      this._ackFns[ev.scope][ev.code] = null
    this._ackFns[ev.scope][ev.code] = {sender,fn, dataOnly}
  }

  netStream(e)
  {
    if (!this._ackReplays[e.ackId()])
    {
      log.warn('stream for not existing ackId',e.toObject())
      return
    }
    let dataOnly = this._ackReplays[e.ackId()].dataOnly
    log.log('net stream',[dataOnly,e.toObject()])
    if (e.type() === 'ackvalue')
    {
      if (dataOnly)
        this._ackReplays[e.ackId()].obs.emit(e.data())
      else
        this._ackReplays[e.ackId()].obs.emit(e)
      return
    }
    if (e.type() === 'ackerror')
    {
      if (dataOnly)
        this._ackReplays[e.ackId()].obs.error(e.data())
      else
        this._ackReplays[e.ackId()].obs.error(e)
      return
    }
    if (e.type() !== 'ackend')
      log.warn('unknown stream event',e.toObject())
    this._ackReplays[e.ackId()].obs.end()
    delete this._ackReplays[e.ackId()]
  }

  /* helpers */
  eventFromNetObject(evObj)
  {
    //console.log(evObj)
    const EventClass = this.factoryClass('eventTypes',evObj.data.type)
    let e = new EventClass(this)
    e.fromObject(evObj)
    log.log('network event',e.toObject())
    return e
  }

  eventFromAckEvent(sender,etype,acke)
  {
    const EventClass = this.factoryClass('eventTypes',etype)
    let ret = new EventClass(sender)
    ret.ackId(acke.ackId())
    ret.reScope(acke.sender().rpc)
    ret.code(acke.code())
    return ret
  }


}

module.exports = F.register(EventsModule)
