const F = require('../factory')
const log = F.log.m('AckEvent')

const AbstractEvent = require('./abstract')

class AckEvent extends F.extends(AbstractEvent)
{

  static info()
  {
    return {
      code: 'ack',
      name: 'Ack system event',
      description: 'Ack system event object requires receiver and reply and may generate error on failed delivery'
    }
  }
  
  constructor(...args)
  {
    super(...args)
    this._ackId = -1
  }
  
  ackId(next)
  {
    if (typeof next !== 'undefined')
      this._ackId = next
    return this._ackId
  }

  toObject()
  {
    let ret = super.toObject()
    ret.data.ackId = this.ackId()
    return ret
  }

  fromObject(obj)
  {
    super.fromObject(obj)
    this.ackId(obj.data.ackId)
  }

}

module.exports = F.register(AckEvent)
