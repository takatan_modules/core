const F = require('../factory')
const log = F.log.m('AckValueEvent')

const AckEvent = require('./event.ack')

class AckValueEvent extends F.extends(AckEvent)
{

  static info()
  {
    return {
      code: 'ackvalue',
      name: 'Ack value system event',
      description: 'Ack value system event object contains stream value'
    }
  }
  
  toObject()
  {
    let ret = super.toObject()
    ret.data.ackId = this.ackId()
    return ret
  }

}

module.exports = F.register(AckValueEvent)
