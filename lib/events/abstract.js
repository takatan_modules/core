const F = require('../factory')
const log = F.log.m('AbstractEvent')

class AbstractEvent extends F.extends('AbstractCoreObject')
{

  static info()
  {
    return {
      code: 'abstract',
      name: 'Abstract system event',
      description: 'Abstract system event object. Contains event, sender and reciver scope data'
    }
  }

  constructor(sender)
  {
    super(sender._core)
    this._sender = {
      rpc: this.M('rpc') ? this.M('rpc').rpcCode() : '#',
      meta: sender //object or object meta
    }
    this._data = {
      type: this.constructor.info().code,
      code: '',
      data: {}
    }
    // # - local process only
    // @ - local process and core (if worker)
    // @@ - everyone possible
    // @{core|workerCode} - to core or rpc worker with code {workerCode}
    this._scope = '@'
  }

  type() { return this.constructor.info().code }
  scope() { return this._scope }
  code(next) {
    if (typeof next !== 'undefined')
      this._data.code = next
    return this._data.code
  }
  data(next) {
    if (typeof next !== 'undefined')
      this._data.data = next
    return this._data.data
  }
  sender() { return this._sender }
  senderRPC() { return this._sender.rpc }

  reScope(next)
  {
    if (typeof next !== 'string'||!next.length)
    {
      //reset to default
      this._scope = '@'
    } else {
      this._scope = next
    }
    return this._scope
  }

  fill(data,scope)
  {
    if (typeof scope === 'string' && scope.length)
      this._scope = scope
    let type = this.constructor.info().code
    if (typeof data === 'string')
      data = {
        code: data,
        type: type
      }
    data.type = type
    this._data = data
    if (typeof this._data.code !== 'string'||!this._data.code.length)
      this._data.code = type+'.undefined'
  }

  toObject()
  {
    let meta = this._sender.meta
    if (F.class('AbstractCoreObject').isPrototypeOf(this._sender.meta.constructor))
      meta = this._sender.meta.meta()
    return {
      sender:{
        rpc: this._sender.rpc,
        meta: meta
      },
      data: this._data,
      scope: this._scope
    }
  }

  fromObject(other)
  {
    if (!other||!other.sender||!other.data||!other.scope||!other.data.code)
      return false
    this._sender = other.sender
    this._scope = other.scope
    this._data = {
      type: other.data.type,
      code: other.data.code,
      data: other.data.data
    }
    return true
  }
}

module.exports = F.register(AbstractEvent)
