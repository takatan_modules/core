const chalk = require('chalk')
const log = require('./log')

/**
*   Console Log object class.
*   uses 'chalk' for colorizing output
*   uses log object as events source
*/
class ConsoleLog
{

  constructor()
  {
    this._colors = {
      trace: chalk.dim.underline,
      silent: chalk.dim,
      all: chalk.blue,
      log: chalk.blue,
      debug: chalk.yellow,
      success: chalk.green,
      info: chalk.cyan,
      test: chalk.magenta,
      warn: chalk.black.bgYellow,
      error: chalk.black.bgRed,
      fatal: chalk.black.bold.bgRed,
      off: chalk.dim,
    }

    this._format = 'dd/mm/yyyy hh:ii:ss zzz'
    this._pretty=false
    this._level = log.LEVELS.ALL

    Object.keys(log._levels).forEach(l=>{
      log.on(l==='error'?'err':l,vars=>{
        let str = chalk.white(this.date())+' ['+this._colors[l](vars.key)+'] '+chalk.dim(vars.msg)
        this._clog(l,str,vars.obj)
      })
    })
  }

  setLevel(lvl)
  {
    this._level = lvl
  }

  date()
  {
    let dt = new Date()
    let dd = dt.getDate().toString()
    let mm = (dt.getMonth()+1).toString()
    let yy = dt.getFullYear().toString()
    let hh = dt.getHours().toString()
    let ii = dt.getMinutes().toString()
    let ss = dt.getSeconds().toString()
    let zz = dt.getMilliseconds().toString()
    dd = dd.length > 1 ? dd : '0'+dd
    mm = mm.length > 1 ? mm : '0'+mm
    hh = hh.length > 1 ? hh : '0'+hh
    ii = ii.length > 1 ? ii : '0'+ii
    ss = ss.length > 1 ? ss : '0'+ss
    let str = this._format.trim()
    str=str.replace(/dd/g,dd)
    str=str.replace(/mm/g,mm)
    str=str.replace(/yyyy/g,yy)
    str=str.replace(/hh/g,hh)
    str=str.replace(/ii/g,ii)
    str=str.replace(/ss/g,ss)
    str=str.replace(/zzz/g,zz)
    return str
  }

  _clog(tp,msg,obj=null)
  {
    if (this._level>log.LEVELS[tp.toUpperCase()]&&tp!=='trace')
      return
    if (obj)
    {
      if (this._pretty)
        msg += ' <'+JSON.stringify(obj,null,1)+'>'
      else
        msg += ' <'+JSON.stringify(obj)+'>'
    }
    console.log(msg)
  }

}

var consoleLog = new ConsoleLog()

module.exports = consoleLog
