const Kefir = require('kefir')
const AbstractCoreModule = require('../core/abstract/module')

const F = require('../factory')
const log = F.log.m('LogModule')
const clog = require('./console.log')

class LogModule extends F.extends(AbstractCoreModule)
{
  moduleConfigure(cfg)
  {
    this._config = cfg
    clog.setLevel(cfg.logLevel)
    return Kefir.constant(null)
  }
 
  moduleInstall()
  {
    return Kefir.constant({
      logLevel: log.LEVELS.DEBUG
    })
  }

}

module.exports = F.register(LogModule)
