const EventEmitter = require('events')

/**
*   Log object class. EventEmitter.
*   uses 'chalk' for colorizing output
*   has methods log, test, error and success
*/
class Log extends EventEmitter
{

  constructor()
  {
    super()
    this._levels = {
      trace: -2, //half standard
      silent: -1, //rare used, but..
      all: 0, //standard
      log: 2, //rare used, but
      debug: 4, //standard
      success: 6, //takatan
      info: 8, //standard
      test: 10, //takatan
      warn: 12, //standard
      error: 14, //standard
      fatal: 16, //standard
      off: 18 //standard
    }
    this._lbls = {
      trace: '>>', //half standard
      silent: '', //rare used, but..
      all: '..', //standard
      log: '..', //rare used, but
      debug: '??', //standard
      success: 'OK', //takatan
      info: '..', //standard
      test: '??', //takatan
      warn: '!', //standard
      error: '!!', //standard
      fatal: '!!!', //standard
      off: '' //standard
    }
    this.LEVELS = {}
    Object.keys(this._levels).forEach(l=>{
      this.LEVELS[l.toUpperCase()] = this._levels[l]

      this[l] = (key,msg=null,obj=null) => {
        let vars = this.vars(this._lbls[l],key,msg,obj)
        /**
        if (l==='trace')
        {
          //const trace = this._trace()
          //console.log(trace[0])
          vars.obj = {
            ...vars.obj,
            file: trace.file,
            path: trace.path,
            method: trace.method,
            line: trace.line
          }
        }
        /**/
        this.emit(l==='error'?'err':l,vars)
      }
    })
  }

  m(m)
  {
    let ret = {
      LEVELS: this.LEVELS,
      l: this
    }
    Object.keys(this._levels).forEach(l=>{
      ret[l] = (...a)=>{ this[l](m,...a) }
    })
    return ret
  }

  vars(defKey,key,msg=null,obj=null)
  {
    let ret = {
      key: defKey,
      msg: '',
      obj: null
    }
    if (!msg) {
      ret.msg = key 
    } else {
      if (typeof msg != 'string')
      {
        ret.msg = key
        ret.obj = msg
      } else {
        if (msg)
        {
          ret.key = key
          ret.msg = msg
        }  else {
          ret.msg = key
        }
        if (obj)
          ret.obj = obj
      }
    }
    return ret
  }

  /**
  _trace()
  {
    let stack = null
      , error = null
      , getting_the_stack = null

    //Error.original_prepareStackTrace = Error.prepareStackTrace
    if(Error.original_prepareStackTrace) // Should be quite rare
      console.error('Traceback error detected: Error.original_prepareStackTrace exists')
    else
      Error.original_prepareStackTrace = Error.prepareStackTrace

    getting_the_stack = {}
    try {
      //console.error('Beginning capture')
      Error.captureStackTrace(getting_the_stack)
      Error.prepareStackTrace = (er,stack)=>{
        if(getting_the_stack && er === getting_the_stack)
          return stack;

        // At this point something has gone wrong. Try to recover the existing prep function.
        if(Error.original_prepareStackTrace) {
          //console.error('Restoring original prepareStackTrace')
          Error.prepareStackTrace = Error.original_prepareStackTrace
          delete Error.original_prepareStackTrace
          return Error.prepareStackTrace(er, stack)
        }

        //console.error('Returning to normal (deleted) Error.prepareStackTrace')
        delete Error.prepareStackTrace
        return stack
      }
      stack = getting_the_stack.stack // This actually calls the prepareStackTrace function
    } catch (capture_er) {
      //console.error('= Capture error =')
      error = capture_er
    } finally {
      getting_the_stack = null
      Error.prepareStackTrace = Error.original_prepareStackTrace // TODO could this ever fail?
      delete Error.original_prepareStackTrace
    }
    if(error)
      throw error
    else if(stack)
      return stack
    else
      throw new Error('Unknown result getting the stack')
  }
  /**/

}

var log = new Log()
module.exports = log
