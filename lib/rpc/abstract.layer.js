const Kefir = require('kefir')
const F = require('../factory')
const log = F.log.m('RPC.AbstractRpcLayer')

class AbstractRpcLayer extends F.extends('AbstractCoreObject')
{

  static info()
  {
    return {
      code: 'abstractrpclayer'
    }
  }

  constructor(parent)
  {
    super(parent)
    this._ready = false
  }

  ready(next) 
  {
    if (typeof next === 'boolean')
      this._ready = next
    return this._ready
  }

  start()
  {
    return Kefir.constantError({error:'AbstractRpcLayer.start() should be reimplemented by child classes'})
  }

  stop()
  {
    return Kefir.constantError({error:'AbstractRpcLayer.stop() should be reimplemented by child classes'})
  }

  onRpcNotification(evJSON)
  {
    let e = this.M('events').eventFromNetObject(evJSON)
    if (e.scope()===this.M('rpc').rpcCode() ||
        e.scope()==='@@' ||
        (e.scope()==='@'&&this.M('rpc').rpcCode()==='core'))
      this.M('events').netNotification(e)
    if (this.M('rpc').rpcCode()==='core'&&e.scope()==='@@')
      this.sendNotificationEvent(e,[e.sender().rpc])
  }

  onRpcAck(evJSON,cb)
  {
    let e = this.M('events').eventFromNetObject(evJSON)
    //log.log('rpc:ack',e.meta())
    let reply = this.M('events').netAckStream(e)
    if (!reply)
    {
      log.warn('rpc:ack reply not exists',evObject)
      if (typeof cb === 'function')
        cb({error:{code:'rpc.stream_not_found'}})
      return
    }
    if (typeof cb === 'function')
      cb(true) //really only {error:....} is error. any other is success
    let arg = e
    if (reply.dataOnly)
      arg = e.data()
    reply.fn(arg).observe(val=>{
      let sev = this.M('events').eventFromAckEvent(reply.sender,'ackvalue',e)
      sev.data(val)
      this.M('rpc').sendStreamEvent(sev)
    },err=>{
      let sev = this.M('events').eventFromAckEvent(reply.sender,'ackerror',e)
      sev.data(val)
      this.M('rpc').sendStreamEvent(sev)
    },()=>{
      let sev = this.M('events').eventFromAckEvent(reply.sender,'ackend',e)
      this.M('rpc').sendStreamEvent(sev)
    })
  }

  onRpcStream(evJSON)
  {
    let e = this.M('events').eventFromNetObject(evJSON)
    this.M('events').netStream(e)
  }

  /* sending methods should be reimplemented by layers modules */

  sendNotificationEvent(e,exclude=[])
  {
    log.error('AbstractRpcLayer.sendNotificationEvent() should be reimplemented by child classes')
  }

  sendStreamAckEvent(e)
  {
    return Kefir.constantError({error:'AbstractRpcLayer.sendStreamAckEvent() should be reimplemented by child classes'})
  }

  sendStreamEvent(e)
  {
    log.error('AbstractRpcLayer.sendStreamEvent() should be reimplemented by child classes')
  }

}

module.exports = F.register(AbstractRpcLayer)
