const Kefir = require('kefir')

//const AbstractCoreModule = require('../core/abstract/module')

const F = require('../factory')
const log = F.log.m('RPCModule')

//const AbstractRpcInterface = require('./abstract.interface')
const AbstractRpcLayer = require('./abstract.layer')

class RPCModule extends F.extends('AbstractFactoryCoreModule')
{

  constructor(...args)
  {
    super(...args)

    this.factoryInit('layers',AbstractRpcLayer)
    //layer classes
    this._layer = null
    //network
    this._net = {
    
    }
    //not initialized config!
    //to be compatible with events module
    //on early initialization stage
    this._config = {
      mode: '#'
    }
    //main ready flag
    //includes layer choise, connection, handshake
    //and post-initialization
    this._ready = false
    //we need check init step - layer can be registered
    //before init, or after, but rpc should start when module 
    //inited and layer defined
    this._inited = false
  }

  layer() { return this._layer }
  rpcCode() { return this.moduleConfig().mode }

  moduleInit()
  {
    return Kefir.constant(this._config)
      .flatMap(cfg=>{
        let Cls = this.factoryClass('layers',cfg.layer)
        let inf = Cls.info()
        if (inf.code === 'abstractrpclayer')
        {
          log.warn('required layer is not registered! starting without!',cfg.layer)
          return Kefir.constant(true)
        }
        return this.startLayer()
      })
  }


  moduleInstall()
  {
    return Kefir.constant({
      mode: 'core',
      layer: 'websocket'
    })
  }


  registerLayer(Layer)
  {
    let inf = Layer.info()
    log.info('registering layer',inf.code)
    this.factoryRegisterClass('layers',Layer)
    if (!this._inited)
      return
    let cfg = this.moduleConfig()
    if (cfg.layer !== inf.code)
      return
    log.test('starting rpc layer on registration',cfg.layer)
    this.startLayer().observe(v=>{
      log.info('layer started')
    },err=>{
      log.error('layer start failed',err)
    })
  }

  startLayer()
  {
    return Kefir.stream(obs=>{
      let cfg = this.moduleConfig()
      log.test('starting rpc layer',{layer:cfg.layer,mode:this.rpcCode()})
      let Layer = this.factoryClass('layers',cfg.layer)
      this._layer = new Layer(this)
      this.obsEnd(obs)
    })
    .flatMap(()=>{
      return this.layer().start()
    })
    //.takeErrors(1)
    .map(_=>{
      log.success('rpc layer started',this.moduleConfig().layer)
    })
  }

  sendStreamAckEvent(e)
  {
    if (!this.layer())
      return
    return this.layer().sendStreamAckEvent(e)
  }

  sendStreamEvent(e)
  {
    if (!this.layer())
      return
    return this.layer().sendStreamEvent(e)
  }

  sendNotificationEvent(e,exclude=[])
  {
    if (!this.layer())
      return
    return this.layer().sendNotificationEvent(e,exclude)
  }
 
}

module.exports = F.register(RPCModule)
