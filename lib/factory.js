const AbstractCoreObject = require('./core/abstract/object')
const log = require('./log/log')
require('./log/console.log')

var __fac_classes = {AbstractCoreObject}
var __fac_mixins = []

function __fac_register(Cls)
{
  __fac_classes[Cls.name] = Cls
  return Cls
}

function __fac_copyProps(target, source)
{ 
  // this function copies all properties and symbols, filtering out some special ones
  Object.getOwnPropertyNames(source)
    .concat(Object.getOwnPropertySymbols(source))
    .forEach((prop) => {
      if (!prop.match(/^(?:constructor|prototype|arguments|caller|name|bind|call|apply|toString|length)$/))
        Object.defineProperty(target, prop, Object.getOwnPropertyDescriptor(source, prop));
    })
}


function __fac_mix(mixFor,mixin)
{
  //console.log('__fac_mix',mixFor,mixin)
  let mfor = ['']
  if (Array.isArray(mixFor)&&typeof mixin === 'object')
    mfor = mixFor
  if (typeof mixFor === 'string'&&(typeof mixin === 'object' || typeof mixin === 'function'))
    mfor = [mixFor]
  if (typeof mixin === 'undefined' && (typeof mixFor === 'object' || typeof mixFor === 'function'))
    mixin = mixFor
  mfor.forEach(mf=>{
    __fac_mixins.push({mf,mixin})
  })
  return mixin
}

function __fac_class(cls)
{
  let ret = null
  if (typeof cls === 'string')
    ret = __fac_classes[cls]
  if (typeof cls === 'function')
  {
    if (!!__fac_classes[cls.name])
      ret = __fac_classes[cls.name]
    else
      ret = __fac_register(cls)
  }
  if (!ret) return ret
  ret.__mixins = []
  __fac_mixins.forEach((mix) => {
    if (mix.mf!==''&&mix.mf!==ret.name)
      return
    if (typeof mix.mixin === 'object')
      __fac_copyProps(ret.prototype,mix.mixin);
    else
      ret.__mixins.push(mix)
  });
  return ret
}

function __fac_extends(...classes)
{
  const baseClass = __fac_class(classes.shift())
  //console.log('__fac_extend',classes,baseClass)
  const mixins = [...classes.map(x=>__fac_class(x)),...baseClass.__mixins.map(x=>x.mixin)]
  class base extends baseClass {
    constructor (...args) {
      super(...args);
      mixins.forEach((mixin) => {
        let nm = (new mixin(...args))
        __fac_copyProps(this,nm);
      });
    }
  }
  mixins.forEach((mixin) => {
    __fac_copyProps(base.prototype, mixin.prototype);
    __fac_copyProps(base, mixin);
  });
  return base;
}

var __fac_app = null

var F = function(coreClass='Core',...startArgs) {
  if (__fac_app) return __fac_app
  const Cls = __fac_class(coreClass)
  if (!Cls) return null
  __fac_app = new Cls(...startArgs)
  return __fac_app
}

F.log = log
F.mix = __fac_mix
F.class = __fac_class
F.extends = __fac_extends
F.register = __fac_register

module.exports = F
