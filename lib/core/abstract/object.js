const log = require('../../log/log').m('AbstractCoreObject')

class AbstractCoreObject {

  constructor(core=null)
  {
    this._core = core
    if (core&&core._core)
      this._core = core._core
  }

  coreObjectSetCore(core)
  {
    this._core = core
  }

  /*
    each core object supports "meta information"
    static method info() returns "class meta information"
    to return object meta, use meta() method
  */
  static info() {
    return {
      code: 'coreobject',
      name: 'Core Object',
      description: 'Core takatan objects'
    }
  }

  /*
    meta returns info by default
  */
  meta() {
    let inf = this.constructor.info()
    if (inf.code)
    {
      inf.type = inf.code+''
      delete inf.code
    }
    return inf
  }

  /*
    serialization
  */
  toObject() { return {} }
  // returns true/false on success restoring from object
  fromObject(other) { return true }
  toObjectRx() { return Rx.of(this.toObject()) }
  fromObjectRx(other) { return Rx.of(this.fromObject(other)) }

  /*
    Kefir.Observer helpers
  */
  obsEnd(obs,ret)
  {
    if (!obs||typeof obs.emit !== 'function')
      log.error('pass obs to obsEnd, cmon')
    obs.emit(ret)
    obs.end()
  }

  obsErr(obs,err)
  {
    //console.log('obsErr',obs,err)
    if (!obs||typeof obs.emit !== 'function')
      log.error('pass obs to obsEnd, cmon')
    obs.error(err)
    obs.end(err)
  }

}

module.exports = AbstractCoreObject
