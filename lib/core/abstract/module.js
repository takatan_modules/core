const Kefir = require('kefir')

const F = require('../../factory')


/**
Abstract Core Module class - base parent class for any module, with default implementation of module behaviour.
@extends module:core/core.CoreObject
@interface
@memberof module:core/core
*/
class AbstractCoreModule extends F.extends('AbstractCoreObject')
{

  static info()
  {
    return {
      name: 'Abstract Core Module',
      code: 'abstractcoremodule',
      description: 'This is just abstract module info. You should never see this message, because each module should reimplement info method',
    }
  }

  moduleReInfo(inf)
  {
    this._moduleInfo = inf
  }

  meta()
  {
    let inf = this.constructor.info()
    let ret = {
      name: inf.name,
      type: inf.code,
      description: inf.description
    }
    if (this._moduleInfo)
    {
      ret = { ...ret, ...this._moduleInfo }
    }
    return ret
  }

  /**
  @constructor
  @param {module:core/core.CoreObject} parent - parent object, to get core objects api
  */
  constructor(parent)
  {
    super(parent)
    /**
      @private
      @name CoreModule#_config - module config, stored at files/modules.json
    */
      //@property {any} _config - module config, saved to files/modules.json
    this._config = null
    this._moduleInfo = null
  }

  /**
  Default module install method. Should be reimplemented by childs, if any install procedure required.
  This method calls only on installation routine and should return module config object, if module requires hold configs at /config/modules.json
  This method calls before {@link CoreModule#moduleConfigure} and before {@link CoreModule#moduleDefaults}
  @param {Object} [withConfig=null] - configuration may passed in installation
  @return {Object} [config=null] - default module config
  */
  moduleInstall(withConfig=null)
  {
    return Kefir.constant(withConfig)
  }

  moduleUninstall()
  {
    return Kefir.constant(null)
  }

  /**
  Default module data creation
  */
  moduleDefaults()
  {
    return Kefir.constant(null)
  }

  /**
  Module configuration routine
  On reimplementation developer should care to save config data to {@link CoreModule._config}
  @param {any} [conf] - config object, loaded from /config/modules.json on 'normal start' or returned by {@link CoreModule#moduleInstall} routine
  */
  moduleConfigure(conf)
  {
    return Kefir.stream(obs=>{
      this._config = conf
      this.obsEnd(obs)
    })
  }

  /**
  Public method to get module config on runtime
  @return {object} - module config object
  */
  moduleConfig()
  {
    return this._config
  }

  /**
  Module load routine, calls after module object was created on loading
  */
  moduleLoad()
  {
    return Kefir.constant(null)
  }

  /**
  Module register routine, calls after all modules were loaded. This is stage to call any factory regigister
  */
  moduleRegister()
  {
    return Kefir.constant(null)
  }

  /**
  Module init routine, calls on start routine after after all modules were loaded and configured
  */
  moduleInit()
  {
    return Kefir.constant(null)
  }

  /**
  Module start routine, calls on start routine after after all modules were initialized
  */
  moduleStart()
  {
    return Kefir.constant(null)
  }

  /**
  Module stop routine, calls on module stop
  */
  moduleStop()
  {
    return Kefir.constant(null)
  }

  /**
  Module update routine, calls on core start, or module manager update events
  @param {string} fromVersion - version, installed in system
  @param {string} toVersion - current module version, mainly
  @return {string} version, to save at /config/modules.json as installed current
  */
  moduleUpdate(fromVersion,toVersion)
  {
    return Kefir.stream(obs=>{
      let list = []
      let vrsf = fromVersion.split('.')
      let fromMaj = vrsf[0] || 0
      let fromMin = vrsf[1] || 0
      let fromPatch = vrsf[2] || 0
      let curMaj = fromMaj
      let curMin = fromMin
      let curPatch = fromPatch
      let vrst = toVersion.split('.')
      let toMaj = vrst[0] || 0
      let toMin = vrst[1] || 0
      let toPatch = vrst[2] || 0
      let done = false
      //let patchDiff = toPatch - curPatch
      //let minDiff = toMin - curMin
      //let maxDiff = toMax - curMax
      let havePatch = curPatch!==toPatch
      let haveMin = curMin!==toMin
      let haveMaj = curMaj!==toMaj
      //how possible?
      if (!havePatch&&!haveMin&&!haveMaj)
        return this.obsEnd(obs,{ver:toVersion})

      //let's iterate with patch version
      //it should change one by one if all correct
      for (let i=fromPatch;i<toPatch;i++)
      {
        let curv = `${curMaj}${curMin}${curPatch}`
        if (curPatch===0)
          curv = `${curMaj}${curMin}`
        let nextPatch = i+1
        let nextv = `${curMaj}${curMin}${nextPatch}`
        let patchfn = `_update${curv}to${nextv}`
        if (typeof this[patchfn] === 'function')
        {
          //lucky! only patch update. no min/maj versions changed
          list.push(patchfn)
          curPatch = nextPatch
          continue
        }
        //lets check next minor
        let nextMin = curMin+1
        nextv = `${curMaj}${nextMin}${nextPatch}`
        patchfn = `_update${curv}to${nextv}`
        if (typeof this[patchfn] === 'function')
        {
          //lucky! found update for minor version
          list.push(patchfn)
          curPatch = nextPatch
          curMin = nextMin
          continue
        }
        //do we have major update?
        if (!haveMaj)
          continue  //looks like for this min/patch there is no update
        let nextMaj = curMaj+1
        nextv = `${nextMax}0${nextPatch}` //major version increment resets minor version to zero
        patchfn = `_update${curv}to${nextv}`
        if (typeof this[patchfn] === 'function')
        {
          //lucky! found update for major version
          list.push(patchfn)
          curPatch = nextPatch
          curMin = 0
          curMaj = nextMaj
          continue
        }
        //so, no any "next" combination found for update
        //let's just go to next patch counter
      }
      //ok.. we should have list of updates now
      if (!list.length)
        return this.obsEnd(obs,{ver:toVersion})
      list.forEach(fn=>{
        obs.emit({fn:fn})
      })
      obs.end()
    }).flatMapConcat(upd=>{
      if (upd.ver)
        return Kefir.constant(upd.ver)
      if (upd.fn)
        return this[upd.fn]
    }).last()
  }


}

module.exports = F.register(AbstractCoreModule)
