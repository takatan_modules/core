const F = require('../../factory')
const log = F.log.m('AbstractFactoryCoreModule')

/**
Abstract Factory class - base parent class for any class, with some factory.
@extends module:core/core.AbstractCoreModule
@interface
@memberof module:core/core
*/
class AbstractFactory extends F.extends('AbstractCoreObject')
{

  static info()
  {
    return {
      name: 'Abstract Factory',
      //code can be npm module under @takatan/{code}
      //or can be user private module at {cwd}/modules/{code} folder
      code: 'abstractfactory',
      description: 'This is just abstract factory class info. You should never see this message, because each child should reimplement info method'
    }
  }

  /**
  @constructor
  @param {module:core/core.CoreObject} parent - parent object, to get core objects api
  */
  constructor(parent=null)
  {
    super(parent)

    /**
      @private
      @name CoreModule#_factories - factories info holder
    */
    this._factories = {
      //{fcode}: {
        //base: Class,
        //field: 'infoFieldCode',
        //classes: {}
      //}
    }
  }

  /**
  Factory module set factory base (class) method.
  @param {string} fcode - factory code
  @param {class} BaseClass - class to use as factory base
  @param {string} fieldInfoCode='code' - Class.info()[fieldInfoCode]
  */
  factoryInit(fcode,BaseClass,infoFieldCode='code')
  {
    this._factories[fcode] = {
      base: BaseClass,
      field: infoFieldCode,
      classes: {}
    }
  }

  /**
  Factory module register factory class
  @param {string} fcode - factory code
  @param {class} BaseClass - class to register (should be inherited from base)
  */
  factoryRegisterClass(fcode,Cls)
  {
    if (!this._factories[fcode])
    {
      log.error('factory base is not initialized')
      return
    }
    if (!this._factories[fcode].base.isPrototypeOf(Cls))
    {
      log.error('registred class is not inherited from factory base')
      return
    }
    let info = Cls.info()
    this._factories[fcode].classes[info[this._factories[fcode].field]] = Cls
  }

  /**
  Factory module getter class
  @param {string} fcode - factory code
  @param {class} BaseClass - class to register (should be inherited from base)
  */
  factoryClass(fcode,typeCode)
  {
    if (!this._factories[fcode]) return null
    return this._factories[fcode].classes[typeCode] || this._factories[fcode].base
  }

  factoryClasses(fcode)
  {
    if (!this._factories[fcode]) return []
    return Object.keys(this._factories[fcode].classes).map(ccode=>{
      return this._factories[fcode].classes[ccode].info()
    })
  }

}

module.exports = F.register(AbstractFactory)
