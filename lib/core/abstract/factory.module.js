const F = require('../../factory')

const AbstractCoreModule = require('./module')
const AbstractFactory = require('./factory')

/**
Abstract Factory Core Module class - base parent class for any module, with some factory.
@extends module:core/core.AbstractCoreModule
@interface
@memberof module:core/core
*/
class AbstractFactoryCoreModule extends F.extends(AbstractCoreModule,AbstractFactory)
{

  static info()
  {
    return {
      name: 'Abstract Factory Core Module',
      //code can be npm module under @takatan/{code}
      //or can be user private module at {cwd}/modules/{code} folder
      code: 'abstractfactorycoremodule',
      description: 'This is just abstract module info. You should never see this message, because each module should reimplement info method'
    }
  }

}

module.exports = F.register(AbstractFactoryCoreModule)
