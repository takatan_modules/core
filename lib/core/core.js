const Kefir = require('kefir')
const F = require('../factory')
const log = F.log.m('Core')
const ModuleManager = require('../module_manager/module')

class Core extends F.extends('AbstractCoreObject')
{
  constructor()
  {
    super()
    this.coreObjectSetCore(this)
    this._mm = new ModuleManager(this)
  }

  static info()
  {
    return {
      code: 'core',
      name: 'System Core',
      description: 'Application core object'
    }
  }

  meta()
  {
    return {
      type: 'core',
      name: 'System core',
      description: 'Application core object'
    }
  }

  module(code)
  {
    return this._mm.moduleObject(code)
  }

  go(callback=null)
  {
    log.log('go',process.cwd())
    let isInstalled = this._mm.isInstalled()
    let stream = Kefir.constant(true)
    if (!this._mm.isInstalled())
      stream = this.install()
    stream
      .flatMap(_=>this._start())
      .takeErrors(1)
      .onValue(_=>{
        log.success('go')
        this.$notify('@core:started')
        if (callback&&typeof callback==='function')
          callback()
      })
      .onError(err=>{
        log.error('go',err)
        if (callback&&typeof callback==='function')
          callback(err)
      })
  }

  _start()
  {
    return this._mm.startNormally()
  }

  install()
  {
    return this._mm.installCore()
  }

  use(modules,configs)
  {
    this._mm.useModules(modules,configs)
  }
  
}

module.exports = F.register(Core)
