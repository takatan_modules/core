const Kefir = require('kefir')
const AbstractCoreModule = require('./abstract/module')

const F = require('../factory')
const log = F.log.m('CoreModule')

class CoreModule extends F.extends(AbstractCoreModule)
{

  moduleUpdateTo1()
  {
    return Kefir.stream(obs=>{
      let rpcConfig = this.M('rpc').moduleConfig()
      log.warn('splitting rpc config',rpcConfig)
      this.obsEnd(obs,rpcConfig)
    })
    .flatMap(cfg=>{
      log.info('installing websocket rpc layer')
      return this.M('module_manager').installNewModule('rpc_websocket',{
        uri: cfg.url,
        handshakeTimeout: cfg.handshakeTimeout
      },{
        noenv: true,
        enabled: true,
        start: false
      })
      .map(_=>cfg)
    })
    /**/
    .flatMap(cfg=>{
      return this.M('module_manager').configureModule('rpc',{
        mode: cfg.mode,
        layer: 'websocket'
      },{saveMDB:true,noenv:true})
      .map(_=>cfg)
    })
    /**/
    .takeErrors(1)
    .map(_=>true)
  }


}

module.exports = F.register(CoreModule)
