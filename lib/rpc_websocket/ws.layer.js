const Kefir = require('kefir')
const F = require('../factory')
const log = F.log.m('RPC.WsRpcLayer')

const Http = require('http')
const SocketIOServer = require('socket.io')
const SocketIOClient = require('socket.io-client')
const url = require('url')

class WsRpcLayer extends F.extends('AbstractRpcLayer')
{

  static info()
  {
    return {
      code: 'websocket',
      name: 'WebSocket RPC Communication Layer'
    }
  }

  constructor(parent)
  {
    super(parent)
    this._ready = false
    this._server = null
    this._io = null
    this._net = {}
  }

  ready(next) 
  {
    if (typeof next === 'boolean')
      this._ready = next
    return this._ready
  }

  start()
  {
    if (this.M('rpc').rpcCode() === 'core')
      return this._startIOServer()
    return this._startIOClient()
  }

  stop()
  {
    return Kefir.constantError({error:'WsRpcWorker.stop() should be reimplemented'})
  }

  _bindSock(sock)
  {
    sock.on('core:whoareyou',cb=>{
      cb(this.M('rpc').rpcCode())
    })

    /* receiving rpc events */
    sock.on('rpc:notification',evJSON=>{
      this.onRpcNotification(evJSON)
    })

    sock.on('rpc:ack',(evJSON,cb)=>{
      this.onRpcAck(evJSON,cb)
    })

    sock.on('rpc:stream',evJSON=>{
      this.onRpcStream(evJSON)
    })

    //websocket layer core also react for proxy events
    if (this.M('rpc').rpcCode()==='core')
    {
      sock.on('rpc:proxyack',(evJSON,cb)=>{
        let e = this.M('events').eventFromNetObject(evJSON)
        if (!this._net[e.scope()])
        {
          log.warn('rpc:proxyack scope not exists',evJSON)
          if (typeof cb === 'function')
            cb({error:{code:'rpc.scope_not_found'}})
          return
        }
        this._net[e.scope()].emit('rpc:ack',evJSON,res=>{
          if (typeof cb === 'function')
            cb(res)
        })
      })

      sock.on('rpc:proxystream',evJSON=>{
        let e = this.M('events').eventFromNetObject(evJSON)
        if (!this._net[e.scope()])
        {
          log.warn('rpc:proxystream scope not exists',evJSON)
          return
        }
        this._net[e.scope()].emit('rpc:stream',evJSON)
      })
    }
  }

  _addWorker(sock)
  {
    sock.emit('core:whoareyou',wcode=>{
      if (!wcode||!wcode.length)
      {
        log.error('worker handshake failed')
        return
      }
      sock.on('disconnect',(err)=>{
        log.warn('worker disconnected',wcode)
        this._net[wcode] = null
        this.$notify('rpc.worker_disconnected',{worker: wcode})
      })
      log.info('worker connected',wcode)
      this._net[wcode] = sock
      this._bindSock(sock)
      this.$notify('rpc.worker_connected',{worker: wcode})
    })
  }

  _startIOClient()
  {
    return Kefir.stream(obs=>{
      log.test('starting as client',this.M('rpc').rpcCode())

      let iourl = null
      iourl = url.parse(this.M('rpc_websocket').moduleConfig().uri)

      if (!iourl.protocol.startsWith('ws'))
        return this.obsErr(obs,{code: 'RPC.PROTOCOL_NOT_SUPPORTED'})

      this._io = SocketIOClient(this.M('rpc_websocket').moduleConfig().uri,{
        autoConnect:false
      })
      
      let done = false
      this._io.once('connect_error',e=>{
        if (done) return
        done = true
        log.error('rpc connect error')
        this.obsErr(obs,e)
      })
      this._io.once('connect',_=>{
        if (done) return
        done = true
        log.success('rpc connected',this.M('rpc_websocket').moduleConfig().uri)
        this.obsEnd(obs)
      })
      this._io.on('connect',_=>{
        log.success('core connected',this.M('rpc_websocket').moduleConfig().uri)
        this.$notify('rpc.core_connected')
      })
      this._io.on('disconnect',_=>{
        log.warn('core disconnected',this.M('rpc_websocket').moduleConfig().uri)
        this.$notify('rpc.core_disconnected')
      })
      this._bindSock(this._io)
      try {
        log.test('connecting to core',this.M('rpc_websocket').moduleConfig().uri)
        this._io.connect()
      } catch (e) {
        done = true
        log.error('rpc connection error')
        this.obsErr(obs,e)
      }
    })
  }

  _startIOServer()
  {
    return Kefir.stream(obs=>{
      log.test('starting as core')
      this._server = Http.createServer()
      this._io = SocketIOServer(this._server)
      //routes
      let iourl = null
      iourl = url.parse(this.M('rpc_websocket').moduleConfig().uri)

      if (!iourl.protocol.startsWith('ws'))
        return this.obsErr(obs,{code: 'RPC.PROTOCOL_NOT_SUPPORTED'})
      
      this._io.on('connection',sock=>{
        this._addWorker(sock)
      })

      let done = false
      this._server.once('error',e=>{
        if (done) return
        done = true
        log.error('rpc _server error')
        this.obsErr(obs,e)
      })
      try {
        this._server.listen({
          host: iourl.hostname,
          port: iourl.port
        },(err)=>{
          done = true
          log.success('rpc listening',this.M('rpc_websocket').moduleConfig().uri)
          this.obsEnd(obs)
        })
      } catch (e) {
        done = true
        log.error('rpc listen error')
        this.obsErr(obs,e)
      }
    })
  }
  /**/

  sendNotificationEvent(e,exclude=[])
  {
    if (this.M('rpc').rpcCode()!=='core')
    {
      //if we are not core - everithing is simple
      //just send it to core
      this._io.emit('rpc:notification',e.toObject())
      return
    }
    Object.keys(this._net).forEach(node=>{
      if (this._net[node]&&!exclude.includes(node)&&(e.scope()==='@@'||e.scope()===node))
        this._net[node].emit('rpc:notification',e.toObject())
    })
  }

  sendStreamAckEvent(e)
  {
    return Kefir.stream(obs=>{
      if (this.M('rpc').rpcCode()==='core')
      {
        //if we are core - send normally if scope(=receiver) connected
        if (!this._net[e.scope()])
          return this.obsErr(obs,{code:'rpc.scope_not_found'})
        this._net[e.scope()].emit('rpc:ack',e.toObject(),res=>{
          if (res&&res.error)
            return this.obsErr(obs,res.error)
          this.obsEnd(obs,res)
        })
      } else {
        //if we are not core - have options
        if (e.scope()==='@'||e.scope()==='core')
          this._io.emit('rpc:ack',e.toObject(),res=>{
            if (res&&res.error)
              return this.obsErr(obs,res.error)
            this.obsEnd(obs,res)
          })
        else
          this._io.emit('rpc:proxyack',e.toObject(),res=>{
            if (res&&res.error)
              return this.obsErr(obs,res.error)
            this.obsEnd(obs,res)
          })
      }
    })
  }

  sendStreamEvent(e)
  {
    if (this.M('rpc').rpcCode()==='core')
    {
      //if we are core - send normally if scope(=receiver) connected
      if (!this._net[e.scope()])
      {
        log.warn('sending stream event after successfull ack and cant found receiver?',e.toObject())
        return
      }
      this._net[e.scope()].emit('rpc:stream',e.toObject())
    } else {
      //if we are not core - have options
      if (e.scope()==='@'||e.scope()==='core')
        this._io.emit('rpc:stream',e.toObject())
      else
        this._io.emit('rpc:proxystream',e.toObject())
    }
  }

}

module.exports = F.register(WsRpcLayer)
