const Kefir = require('kefir')

const F = require('../factory')
const log = F.log.m('RPCWebSocketLayerModule')

const WsRpcLayer = require('./ws.layer')

class RPCWebSocketLayerModule extends F.extends('AbstractCoreModule')
{

  moduleRegister()
  {
    return Kefir.stream(obs=>{
      this.M('rpc').registerLayer(WsRpcLayer)
      this.obsEnd(obs)
    })
  }

  moduleInstall()
  {
    return Kefir.constant({
      uri: 'ws://0.0.0.0:4114',
      handshakeTimeout: 5000 //ms
    })
  }

}

module.exports = F.register(RPCWebSocketLayerModule)
